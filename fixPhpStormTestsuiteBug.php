<?php

declare(strict_types=1);

$path_to_file = 'vendor/phpunit/phpunit/src/TextUI/CliArguments/Builder.php';
$file_contents = file_get_contents($path_to_file);
$file_contents = str_replace('$testSuite = $option[1];', '$filter = $filter ?? $option[1];', $file_contents);
file_put_contents($path_to_file, $file_contents);