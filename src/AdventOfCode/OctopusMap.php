<?php

namespace AdventOfCode;

class OctopusMap
{
    /**
     * @var Octopus[]
     */
    private array $octopuses = [];
    private int $totalFlashCnt = 0;
    private int $flr = 0;

    public function add(?Octopus $octopus)
    {

        if ($octopus === null) {
            return;
        }
        $this->octopuses[$octopus->getId()] = $octopus;
    }

    public function getOctopusByCoord(int $x, int $y): ?Octopus
    {
        return $this->octopuses[$x . '#' . $y] ?? null;
    }

    public function increase(): int
    {
        $this->flr=0;
        foreach ($this->octopuses as $octopus) {
            $octopus->increase();
        }
        $this->flash();
        $this->resetNumber();
        $this->resetFlash();
        return $this->flr;
    }

    private function resetFlash()
    {
        foreach ($this->octopuses as $octopus) {
            $octopus->resetFlash();
        }
    }

    private function resetNumber()
    {
        foreach ($this->octopuses as $octopus) {
            $octopus->resetNumber();
        }
    }


    private function flash()
    {
        $newFlash = false;
        foreach ($this->octopuses as $octopus) {
            if ($octopus->flash()) {
                $this->totalFlashCnt++;
                $this->flr++;
                $newFlash = true;
            }
        }
        if ($newFlash) {
            $this->flash();
        }
    }

    /**
     * @return int
     */
    public function getTotalFlashCnt(): int
    {
        return $this->totalFlashCnt;
    }
}
