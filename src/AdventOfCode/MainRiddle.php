<?php
declare(strict_types=1);

namespace AdventOfCode;

abstract class MainRiddle
{
    /**
     * @var string[]
     */
    protected array $lines;
    public static int $day = -1;


    public function __construct()
    {
        $this->lines = (new FileReader(sprintf(ROOT_PATH . '/input/day%d.txt', static::$day)))->getLines();
    }


    abstract public function calcResult();

    abstract public function calcResult2();
}
