<?php

namespace AdventOfCode;

class Helper
{
    public static function castElementsToInt(array $input): array
    {
        $filtered = array_filter($input, static fn($n) => trim($n) !== '');
        return array_map(static fn($n) =>  (int)$n, array_values($filtered));

    }
}