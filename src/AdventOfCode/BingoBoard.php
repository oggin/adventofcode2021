<?php

namespace AdventOfCode;

class BingoBoard
{
    private array $rowStack = [];
    private array $colStack = [];
    private static string $PREFIX = '#';

    public function __construct(array $input)
    {
        foreach ($input as $idx => $row) {
            foreach (Helper::castElementsToInt(explode(' ', $row)) as $colIdx => $col) {
                $key = $this->getElementKey($col);
                $this->rowStack[$idx][$key] = false;
                $this->initColumn($colIdx);
                $this->colStack[$colIdx][$key] = false;
            }
        }
    }

    private function getElementKey(int $value): string
    {
        return static::$PREFIX . $value;
    }

    private function removeElementKey(string $value): int
    {
        return (int)str_replace(static::$PREFIX, '', $value);
    }

    public function markValue(int $value): bool
    {
        $this->markValueForStack($this->colStack, $value);
        $this->markValueForStack($this->rowStack, $value);

        return $this->hasWon();
    }

    public function hasWon(): bool
    {
        return $this->checkStack($this->rowStack) || $this->checkStack($this->colStack);

    }

    public function getSumUnChecked(): int
    {
        $sum = 0;
        foreach ($this->colStack as $item) {
            foreach (array_keys($this->getUncheckedItemsForRow($item)) as $key) {
                $sum += $this->removeElementKey($key);
            }
        }

        return $sum;
    }

    private function getUncheckedItemsForRow(array $row): array
    {
        return array_filter($row, static fn($val) => $val === false);
    }

    private function checkStack($stack): bool
    {
        foreach ($stack as $item) {
            if (count($this->getUncheckedItemsForRow($item)) === 0) {
                return true;
            }
        }
        return false;
    }

    private function initColumn($colIdx): void
    {
        if (!array_key_exists($colIdx, $this->colStack)) {
            $this->colStack[$colIdx] = [];
        }
    }

    private function markValueForStack(array &$stack, int $value): void
    {
        foreach ($stack as $idx => $item) {
            if (array_key_exists(($key = $this->getElementKey($value)), $item)) {
                $stack[$idx][$key] = true;
            }
        }

    }

}