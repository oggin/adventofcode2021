<?php

namespace AdventOfCode;

class HeatMapPoint
{
    public static string $U = 'up';
    public static string $D = 'down';
    public static string $L = 'left';
    public static string $R = 'right';
    private int $value;
    private int $xCoord;
    private int $yCoord;
    private array $map;

    public function __construct(array $map, int $yCoord, int $xCoord)
    {
        $this->value = $map[$yCoord][$xCoord];
        $this->map = $map;
        $this->xCoord = $xCoord;
        $this->yCoord = $yCoord;
    }

    public function isLow(): bool
    {

        $u = ($p = $this->get(self::$U)) ? $p->getValue() : PHP_INT_MAX;
        $d = ($p = $this->get(self::$D)) ? $p->getValue() : PHP_INT_MAX;
        $l = ($p = $this->get(self::$L)) ? $p->getValue() : PHP_INT_MAX;
        $r = ($p = $this->get(self::$R)) ? $p->getValue() : PHP_INT_MAX;
        return min($u, $d, $l, $r) > $this->value;
    }

    public function get($direction): ?HeatMapPoint
    {

        if (!$this->has($direction)) {
            return null;
        }

        $newY = $this->yCoord;
        $newX = $this->xCoord;

        if ($direction === self::$U) {
            $newY--;
        }
        if ($direction === self::$D) {
            $newY++;
        }
        if ($direction === self::$L) {
            $newX--;
        }
        if ($direction === self::$R) {
            $newX ++;
        }

        return new HeatMapPoint($this->map, $newY, $newX);
    }

    public function has($direction): ?bool
    {
        if ($direction === self::$U) {
            return $this->yCoord !== 0;
        }
        if ($direction === self::$D) {
            return $this->yCoord !== count($this->map) - 1;
        }
        if ($direction === self::$L) {
            return $this->xCoord !== 0;
        }
        if ($direction === self::$R) {
            return $this->xCoord !== count($this->map[$this->yCoord]) - 1;
        }
        return null;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getBasinCount(&$ret, ?HeatMapPoint $from = null)
    {
        if ($this->value === 9) {
            return;
        }

        $ret[$this->yCoord . '#' . $this->xCoord] = 1;

        foreach ([self::$U, self::$D, self::$L, self::$R] as $direction) {
            if ($this->has($direction) && ($pl = $this->get($direction))->isGreaterThan($this)) {
                if ($from === null || !$from->equals($pl)) {
                    $pl->getBasinCount($ret, $this);
                }
            }
        }
    }

    public function isGreaterThan(HeatMapPoint $other): bool
    {
        return $this->value > $other->getValue();
    }

    public function equals(HeatMapPoint $other): bool
    {
        return $this->xCoord === $other->getXCoord() && $this->yCoord === $other->getYCoord();
    }

    public function getXCoord(): int
    {
        return $this->xCoord;
    }

    public function getYCoord(): int
    {
        return $this->yCoord;
    }
}
