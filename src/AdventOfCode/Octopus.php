<?php

namespace AdventOfCode;

class Octopus
{
    private int $value;
    private string $id;
    private bool $flashed = false;

    /**
     * @var Octopus[]
     */
    private array $neigbours = [];

    public function __construct(int $x, int $y, int $value)
    {
        $this->value = $value;
        $this->id = $x . '#' . $y;
    }

    public function addNeigbour(?Octopus $neighbour, ?Octopus $from = null)
    {
        if ($neighbour === null) {
            return;
        }
        if (!array_key_exists($neighbour->getId(), $this->neigbours)) {
            $this->neigbours[$neighbour->getId()] = $neighbour;
        }
        if ($from === null) {
            $neighbour->addNeigbour($this, $neighbour);
        }
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    public function flash(): bool
    {
        if ($this->value > 9 && !$this->flashed) {
            foreach ($this->getNeigbours() as $neigbour) {
                $neigbour->increase();
            }
            $this->flashed = true;
            return true;
        }
        return false;
    }

    /**
     * @return Octopus[]
     */
    public function getNeigbours(): array
    {
        return $this->neigbours;
    }

    public function increase()
    {
        $this->value++;
    }

    public function resetFlash()
    {
        $this->flashed = false;
    }

    public function resetNumber()
    {
        if ($this->flashed) {
            $this->value = 0;
        }
    }
}
