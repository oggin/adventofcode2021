<?php

namespace AdventOfCode;

class Point
{
    private int $x;
    private int $y;

    public function __construct(string $pointSeparated)
    {
        [$this->x, $this->y] = Helper::castElementsToInt(explode(',', $pointSeparated));
    }

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function getLinePoints(Point $p, bool $onlyEqual): array
    {

        if ($this->x === $p->getX()) {
            return $this->equalX($p);
        }
        if ($this->y === $p->getY()) {
            return $this->equalY($p);
        }

        return $onlyEqual ? [] : $this->differentXandY($p);


    }

    private function equalX(Point $p): array
    {
        $from = min($this->y, $p->getY());
        $to = max($this->y, $p->getY());
        $up = [];
        for ($point = $from; $point <= $to; $point++) {
            $up[] = $this->x . ',' . $point;
        }
        return $up;
    }

    private function equalY(Point $p): array
    {
        $up = [];
        $from = min($this->x, $p->getX());
        $to = max($this->x, $p->getX());
        for ($point = $from; $point <= $to; $point++) {
            $up[] = $point . ',' . $this->y;
        }
        return $up;
    }

    private function differentXandY(Point $p): array
    {
        $up = [];
        $from = $this->x < $p->getX() ? $this : $p;
        $to = $this->x >= $p->getX() ? $this : $p;
        $step = $from->getY() < $to->getY() ? 1 : -1;


        $x = $from->getX();
        $y = $from->getY();
        while ($x <= $to->getX()) {
            $up[] = $x . ',' . $y;
            $y += $step;
            $x++;
        }
        return $up;
    }


}