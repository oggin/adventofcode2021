<?php
declare(strict_types=1);

namespace AdventOfCode\Riddles;

use AdventOfCode\MainRiddle;

class Day24 extends MainRiddle
{

    public static int $day = 24;

    public function calcResult(): int
    {
        return $this->calc(80);
    }

    public function calcResult2(): int
    {
        return $this->calc(256);
    }

    private function calc(int $days): int
    {
        return $days;
    }
}
