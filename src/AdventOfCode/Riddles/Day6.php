<?php
declare(strict_types=1);

namespace AdventOfCode\Riddles;

use AdventOfCode\Helper;
use AdventOfCode\MainRiddle;

class Day6 extends MainRiddle
{

    public static int $day = 6;
    private array $fishesByTimer = [];

    public function calcResult(): int
    {
        return $this->calc(80);
    }

    public function calcResult2(): int
    {
        return $this->calc(256);
    }

    private function calc(int $days)
    {
        $this->initFishStack();

        for ($day = 1; $day <= $days; $day++) {
            $ageZeroCnt = $this->fishesByTimer['#0'];
            for ($i = 0; $i < 8; $i++) {
                $this->fishesByTimer['#' . $i] = $this->fishesByTimer['#' . ($i + 1)];
            }
            $this->fishesByTimer['#8'] = $ageZeroCnt;
            $this->fishesByTimer['#6'] += $ageZeroCnt;
        }
        return array_sum(array_values($this->fishesByTimer));
    }

    private function initFishStack(): void
    {
        $fishTimer = Helper::castElementsToInt(explode(',', $this->lines[0]));
        $cntByDays = array_count_values($fishTimer);

        for ($i = 0; $i <= 8; $i++) {
            $this->fishesByTimer['#' . $i] = $cntByDays[$i] ?? 0;
        }
    }
}
