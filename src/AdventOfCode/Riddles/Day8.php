<?php
declare(strict_types=1);

namespace AdventOfCode\Riddles;

use AdventOfCode\MainRiddle;

class Day8 extends MainRiddle
{

    public static int $day = 8;
    private array $stack5Segments = [];
    private array $stack6Segments = [];
    private array $orderedStack = [];

    public function calcResult(): int
    {
        $cnt =0;
        foreach ($this->lines as $line) {
            $parts = explode('|', $line);
            $input = trim($parts[1]);

            foreach (explode(' ', $input) as $input) {
                $len = strlen(trim($input));
                if (in_array($len, [2,3,4,7])) {
                    $cnt++;
                }
            }
        }
        return $cnt;
    }

    public function calcResult2(): int
    {
        $sum = 0;
        foreach ($this->lines as $line) {
            $parts = explode('|', $line);
            $input = trim($parts[0]);

            $this->orderedStack = $this->stack5Segments = $this->stack6Segments = [];
            foreach (explode(' ', $input) as $input) {
                $this->buildStacks($input);
            }

            $this->mapThree();
            $this->mapTNine();
            $this->mapZero();
            $this->orderedStack[6] = array_pop($this->stack6Segments);
            $this->mapFive();
            $this->orderedStack[2] = array_pop($this->stack5Segments);

            $dictionary = [];
            foreach ($this->orderedStack as $digit => $letters) {
                $dictionary[implode('', $letters)] = $digit;
            }

            $fourDigit ='';
            foreach (explode(' ', trim($parts[1])) as $lookUp) {
                $orderd  = implode('', $this->allCharsNatImplode($lookUp));
                $fourDigit .= $dictionary[$orderd];
            }
            $sum += (int) $fourDigit;
        }


        return $sum;
    }

    private function allCharsNatImplode($w) : array
    {
        $letters = str_split($w);
        natcasesort($letters);
        return $letters;
    }


    private function buildStacks(string $input): void
    {
        $ordered = $this->allCharsNatImplode($input);
        $len = strlen(trim($input));

        $map = [2 => 1, 3 => 7, 4 => 4, 7 => 8];
        if (array_key_exists($len, $map)) {
            $this->orderedStack[$map[$len]] = $ordered;
            return;
        }

        if ($len === 5) {
            $this->stack5Segments[] = $ordered;
        } else {
            $this->stack6Segments[] = $ordered;
        }
    }

    private function mapThree(): void
    {
        foreach ($this->stack5Segments as $idx5 => $segments5) {
            if (count(array_diff($this->orderedStack[7], $segments5)) === 0) {
                $this->orderedStack[3] = $segments5;
                unset($this->stack5Segments[$idx5]);
                return;
            }
        }
    }

    private function mapTNine(): void
    {
        foreach ($this->stack6Segments as $idx => $segments) {
            if (count(array_diff($this->orderedStack[3], $segments)) === 0) {
                $this->orderedStack[9] = $segments;
                unset($this->stack6Segments[$idx]);
                return;
            }
        }
    }

    private function mapZero(): void
    {
        foreach ($this->stack6Segments as $idx => $segments) {
            if (count(array_diff($this->orderedStack[1], $segments)) === 0) {
                $this->orderedStack[0] = $segments;
                unset($this->stack6Segments[$idx]);
                return;
            }
        }
    }

    private function mapFive(): void
    {
        foreach ($this->stack5Segments as $idx5 => $segments5) {
            if (count(array_diff($segments5, $this->orderedStack[9])) === 0) {
                $this->orderedStack[5] = $segments5;
                unset($this->stack5Segments[$idx5]);
                return;
            }
        }
    }
}
