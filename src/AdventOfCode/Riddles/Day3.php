<?php
declare(strict_types=1);

namespace AdventOfCode\Riddles;

use AdventOfCode\MainRiddle;
use Exception;
use InvalidArgumentException;

class Day3 extends MainRiddle
{

    public static int $day = 3;
    private int $idx;
    private int $search;


    public function calcResult()
    {
        $totalLines = count($this->lines);
        $expectedLen = count(str_split($this->lines[0]));
        $digitCnt = array_fill(0, $expectedLen, 0);
        foreach ($this->lines as $idx => $line) {
            $digits = str_split($line);
            $curCount = count($digits);
            if ($curCount !== $expectedLen) {
                throw new InvalidArgumentException(
                    sprintf('row %d has %d digits, %d expected', $idx, $curCount, $expectedLen)
                );
            }
            for ($curIdx = 0; $curIdx < $curCount; $curIdx++) {
                $digitCnt[$curIdx] += $digits[$curIdx];
            }
        }

        $gamma = $epsilon = '';
        $halfLines = (int)($totalLines / 2);
        foreach ($digitCnt as $resultDigit) {
            $gamma .= $resultDigit >= $halfLines ? '1' : '0';
            $epsilon .= $resultDigit < $halfLines ? '1' : '0';
        }

        $gammaDecimal = (int)base_convert($gamma, 2, 10);
        $epsilonDecimal = (int)base_convert($epsilon, 2, 10);
        return $gammaDecimal * $epsilonDecimal;
    }

    /**
     * @return int
     * @throws Exception
     */
    public function calcResult2() :int
    {

        $expectedLen = count(str_split($this->lines[0]));
        $digitCnt = [];
        foreach ($this->lines as $idx => $line) {
            $digits = array_map(static fn($value): int => (int)$value, str_split($line));
            $curCount = count($digits);
            if ($curCount !== $expectedLen) {
                throw new InvalidArgumentException(
                    sprintf('row %d has %d digits, %d expected', $idx, $curCount, $expectedLen)
                );
            }

            $digitCnt[] = $digits;
        }


        $oxy = (int)$this->getOxygen($expectedLen, $digitCnt);
        $scrubber = (int)$this->getScrubber($expectedLen, $digitCnt);
        return $oxy * $scrubber;
    }


    private function getOxygen(int $expectedLen, array $filtered): ?int
    {
        for ($i = 0; $i < $expectedLen; $i++) {
            $this->idx = $i;
            $this->search = $this->hasMoreOf($filtered, $i);
            $filtered = array_filter($filtered, [$this, 'filter']);

            if (count($filtered) === 1) {
                return $this->getBaseConvertedNumber(current($filtered));
            }
        }
        return null;
    }

    private function hasMoreOf(array $data, int $idx): int
    {
        $cnt = $this->getCnt($data, $idx);

        return $cnt[1] >= $cnt[0] ? 1 : 0;
    }

    /**
     * @param array $data
     * @param int $idx
     * @return int[]
     */
    private function getCnt(array $data, int $idx): array
    {
        $cnt = [0 => 0, 1 => 0];
        foreach ($data as $dataset) {
            $cnt[$dataset[$idx]]++;
        }
        return $cnt;
    }


    private function getScrubber(int $expectedLen, array $filtered): ?int
    {
        for ($i = 0; $i < $expectedLen; $i++) {
            $this->idx = $i;
            $this->search = $this->hasLessOf($filtered, $i);
            $filtered = array_filter($filtered, [$this, 'filter']);

            if (count($filtered) === 1) {
                return $this->getBaseConvertedNumber(current($filtered));
            }
        }
        return null;
    }

    private function hasLessOf(array $data, int $idx): int
    {
        $cnt = $this->getCnt($data, $idx);

        return $cnt[0] <= $cnt[1] ? 0 : 1;
    }

    private function filter(array $input): bool
    {
        return $input[$this->idx] === $this->search;
    }

    private function getBaseConvertedNumber(array $result): int
    {
        return (int)base_convert(implode('', $result), 2, 10);
    }
}
