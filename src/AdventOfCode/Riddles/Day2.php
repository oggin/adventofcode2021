<?php

namespace AdventOfCode\Riddles;

use AdventOfCode\DayTwoCommand;
use AdventOfCode\MainRiddle;
use Exception;

class Day2 extends MainRiddle
{
    public static int $day = 2;
    private int $sumForward = 0;
    private int $depth = 0;
    private int $aim = 0;
    private bool $useAim = false;


    /**
     * @return int
     * @throws Exception
     */
    public function calcResult2(): int
    {
        return $this->calculation(true);
    }

    /**
     * @param bool $withAim
     * @return int
     * @throws Exception
     */
    private function calculation(bool $withAim = false): int
    {
        $this->useAim = $withAim;
        $this->resetValues();
        foreach ($this->lines as $line) {
            $this->handleSingleCmd($line);
        }

        $result = $this->sumForward * $this->depth;
        fwrite(STDOUT, sprintf(
            'calculated %d for forward, %d for up/down resulting in %d',
            $this->sumForward,
            $this->depth,
            $result
        ));
        return $result;
    }

    private function resetValues(): void
    {
        $this->aim = 0;
        $this->sumForward = 0;
        $this->depth = 0;
    }

    /**
     * @throws Exception
     */
    private function handleSingleCmd(string $line): void
    {

        $cmd = new DayTwoCommand($line);

        if ($cmd->isUpDown()) {
            if ($this->useAim) {
                $this->aim += $cmd->getAmount();
            } else {
                $this->depth += $cmd->getAmount();
            }

            return;
        }
        $this->sumForward += $cmd->getAmount();
        if ($this->useAim) {
            $this->depth += $cmd->getAmount() * $this->aim;
        }
    }

    /**
     * @return int
     * @throws Exception
     */
    public function calcResult(): int
    {
        return $this->calculation();
    }
}
