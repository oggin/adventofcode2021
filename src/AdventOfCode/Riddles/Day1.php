<?php

declare(strict_types=1);

namespace AdventOfCode\Riddles;

use AdventOfCode\MainRiddle;

class Day1 extends MainRiddle
{

    public static int $day = 1;


    public function calcResult(): int
    {
        $cnt = 0;

        for ($idx = 1, $idxMax = count($this->lines); $idx < $idxMax; $idx++) {
            $prev = (int)$this->lines[$idx - 1];
            $cur = (int)$this->lines[$idx];
            $cnt += ($cur - $prev > 0) ? 1 : 0;
        }


        return $cnt;
    }

    public function calcResult2(): int
    {
        $cnt = 0;

        for ($idx = 3, $idxMax = count($this->lines); $idx < $idxMax; $idx++) {
            $first = (int)$this->lines[$idx - 3];
            $second = (int)$this->lines[$idx - 2];
            $third = (int)$this->lines[$idx - 1];
            $cur = (int)$this->lines[$idx];
            $cnt += ($second + $third + $cur > $first + $second + $third) ? 1 : 0;
        }

        return $cnt;
    }
}
