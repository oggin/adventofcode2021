<?php
declare(strict_types=1);

namespace AdventOfCode\Riddles;

use AdventOfCode\Helper;
use AdventOfCode\MainRiddle;
use AdventOfCode\Octopus;
use AdventOfCode\OctopusMap;

class Day11 extends MainRiddle
{

    public static int $day = 11;
    private OctopusMap $map;

    public function calcResult(): int
    {
        $this->init();

        for ($round = 1; $round <= 100; $round++) {
            $this->map->increase();
        }
        return $this->map->getTotalFlashCnt();
    }

    private function init(): void
    {
        $this->map = new OctopusMap();
        foreach ($this->lines as $y => $line) {
            foreach (Helper::castElementsToInt(str_split($line)) as $x => $value) {
                $octopus = new Octopus($x, $y, $value);
                $this->map->add($octopus);
            }
        }

        foreach ($this->lines as $y => $line) {
            foreach (str_split($line) as $x => $value) {
                $octopus = $this->map->getOctopusByCoord($x, $y);
                $octopus->addNeigbour($this->map->getOctopusByCoord($x - 1, $y - 1));
                $octopus->addNeigbour($this->map->getOctopusByCoord($x, $y - 1));
                $octopus->addNeigbour($this->map->getOctopusByCoord($x + 1, $y - 1));

                $octopus->addNeigbour($this->map->getOctopusByCoord($x + -1, $y));
                $octopus->addNeigbour($this->map->getOctopusByCoord($x + 1, $y));

                $octopus->addNeigbour($this->map->getOctopusByCoord($x - 1, $y + 1));
                $octopus->addNeigbour($this->map->getOctopusByCoord($x, $y + 1));
                $octopus->addNeigbour($this->map->getOctopusByCoord($x + 1, $y + 1));
            }
        }
    }

    public function calcResult2(): int
    {
        $this->init();
        $cnt = 0;
        do {
            $flashed = $this->map->increase();
            $cnt++;
        } while ($flashed !== 100);
        return $cnt;
    }
}
