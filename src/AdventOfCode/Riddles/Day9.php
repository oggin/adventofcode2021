<?php
declare(strict_types=1);

namespace AdventOfCode\Riddles;

use AdventOfCode\HeatMapPoint;
use AdventOfCode\Helper;
use AdventOfCode\MainRiddle;

class Day9 extends MainRiddle
{

    public static int $day = 9;
    private array $map = [];

    public function calcResult(): int
    {
        $this->buildMap();

        return $this->getSumLowPoints();
    }

    public function calcResult2(): int
    {
        $this->buildMap();
        $cnt = [];
        foreach ($this->getLowPoints() as $point) {
            $bc = [];
            $point->getBasinCount($bc);
            $cnt[] = count($bc);
        }
        $cnt = array_unique($cnt);
        rsort($cnt);
        return $cnt[0] * $cnt[1] * $cnt[2];
    }



    private function buildMap(): void
    {
        $this->map = [];
        foreach ($this->lines as $line) {
            $this->map[] = Helper::castElementsToInt(str_split($line));
        }
    }

    /**
     * @return HeatMapPoint[]
     */
    private function getLowPoints() : array
    {
        $points = [];
        foreach ($this->lines as $y => $line) {
            $row = Helper::castElementsToInt(str_split($line));
            foreach ($row as $x => $point) {
                $hm = new HeatMapPoint($this->map, $y, $x);
                if ($hm->isLow()) {
                    $points[]= $hm;
                }
            }
        }
        return $points;
    }

    private function getSumLowPoints()
    {
        $sum = 0;
        foreach ($this->getLowPoints() as $point) {
            $sum+= $point->getValue() +1;
        }
        return $sum;
    }
}
