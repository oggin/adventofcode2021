<?php
declare(strict_types=1);

namespace AdventOfCode\Riddles;

use AdventOfCode\MainRiddle;
use AdventOfCode\Point;

class Day5 extends MainRiddle
{

    public static int $day = 5;

    public function calcResult(): int
    {
        return $this->calc(true);
    }

    public function calcResult2(): int
    {

        return $this->calc(false);
    }

    private function calc(bool $onlyEqual)
    {
        $stack = [];
        foreach ($this->lines as $line) {
            $points = explode(' -> ', $line);
            $p1 = new Point($points[0]);
            $p2 = new Point($points[1]);
            foreach ($p1->getLinePoints($p2, $onlyEqual) as $coord) {
                $stack[$coord] = $stack[$coord] ?? 0;
                $stack[$coord]++;
            }
        }

        return count(array_filter($stack, static fn($v)=> $v>1));
    }
}
