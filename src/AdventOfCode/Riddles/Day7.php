<?php
declare(strict_types=1);

namespace AdventOfCode\Riddles;

use AdventOfCode\Helper;
use AdventOfCode\MainRiddle;

class Day7 extends MainRiddle
{

    public static int $day = 7;

    public function calcResult(): int
    {
        return $this->calc();
    }

    public function calcResult2(): int
    {
        return $this->calc(false);
    }

    private function calc(bool $constantRate = true): int
    {
        $pointByValue = array_count_values(Helper::castElementsToInt(explode(',', $this->lines[0])));
        $uniquePonits = array_keys($pointByValue);
        $costToMoveToLevel = [];

        foreach (range(min($uniquePonits), max($uniquePonits)) as $level) {
            foreach ($uniquePonits as $point) {
                $costToMoveToLevel['#'.$level] = $costToMoveToLevel['#'.$level]?? 0;
                $distance = abs($point - $level);
                if ($constantRate === false) {
                    //use gauss to calculate needed fuel
                    $distance = ($distance * $distance + $distance) / 2;
                }
                //multiply needed fuel by number of crabs at this level
                $costToMoveToLevel['#'.$level] += $distance * $pointByValue[$point];
            }
        }

        return min($costToMoveToLevel);
    }
}
