<?php
declare(strict_types=1);

namespace AdventOfCode\Riddles;

use AdventOfCode\MainRiddle;

class Day10 extends MainRiddle
{

    public static int $day = 10;
    private array $patterns = ['<>', '()', '[]', '{}'];
    private array $open = ['<', '(', '[', '{'];
    private array $closing = ['<' => '>', '(' => ')', '[' => ']', '{' => '}'];
    private array $map = ['>' => 25137, '}' => 1197, ']' => 57, ')' => 3];
    private array $map1 = ['>' => 4, '}' => 3, ']' => 2, ')' => 1];


    public function calcResult(): int
    {
        $chars = [];
        foreach ($this->lines as $line) {
            if ($s = $this->getFirstIllealChar($line)) {
                $chars[$s] = $chars[$s] ?? 0;
                $chars[$s] += $this->map[$s];
            }
        }
        return array_sum($chars);
    }

    private function getFirstIllealChar($str): ?string
    {

        $new = $str;
        $old = '';
        while ($new !== $old) {
            $old = $new;
            foreach ($this->patterns as $repl) {
                $new = str_replace($repl, '', $new);
            }
        }

        foreach (str_split($new) as $char) {
            if (!in_array($char, $this->open)) {
                return $char;
            }
        }

        return null;
    }

    public function calcResult2(): int
    {
        $scores = [];
        foreach ($this->lines as $line) {
            if ($chars = $this->getUnfinised($line)) {
                $score = 0;
                foreach ($chars as $char) {
                    $close = $this->closing[$char];
                    $score = $score * 5 + $this->map1[$close];
                }
                $scores[] = $score;
            }
        }
        sort($scores);
        return $scores[(count($scores)-1)/2];
    }

    private function getUnfinised($str): ?array
    {

        $new = $str;
        $old = '';
        while ($new !== $old) {
            $old = $new;
            foreach ($this->patterns as $repl) {
                $new = str_replace($repl, '', $new);
            }
        }

        foreach (str_split($new) as $char) {
            if (!in_array($char, $this->open)) {
                return null;
            }
        }

        return str_split(strrev($new));
    }
}
