<?php

declare(strict_types=1);

namespace AdventOfCode;

use PHPUnit\TextUI\TestFileNotFoundException;

class FileReader
{

    private array $lines;

    public function __construct(string $filename)
    {
        if (!file_exists($filename)) {
            throw new TestFileNotFoundException($filename);
        }
        $lines = explode("\n", trim((string)@file_get_contents($filename)));
        $this->lines = $lines;
    }

    /**
     * @return string[]
     */
    public function getLines(): array
    {
        return $this->lines;
    }
}
