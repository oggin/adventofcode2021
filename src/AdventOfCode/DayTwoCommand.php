<?php
declare(strict_types=1);

namespace AdventOfCode;

use Exception;

class DayTwoCommand
{
    public static array $allowedCommands = ['up', 'down', 'forward'];
    private int $amount;
    private string $cmd;

    /**
     * @throws Exception
     */
    public function __construct(string $line)
    {

        $lineData = explode(' ', $line);
        if (count($lineData) !== 2) {
            throw new Exception(sprintf('invalid line format "%s"', $line));
        }
        $cmd = strtolower(trim($lineData[0]));
        $amount = (int)$lineData[1];

        if (!in_array($cmd, static::$allowedCommands, true)) {
            throw new Exception('invalid command ' . $cmd);
        }

        $this->cmd = $cmd;
        $this->amount = $amount;

        if ($this->cmd === 'up') {
            $this->amount *= -1;
        }
    }

    public function getAmount(): int
    {

        return $this->amount;
    }

    public function isUpDown(): bool
    {
        return in_array($this->cmd, ['up', 'down']);
    }
}
