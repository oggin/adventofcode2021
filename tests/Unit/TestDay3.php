<?php

declare(strict_types=1);

namespace Unit;

use AdventOfCode\Riddles\Day3;
use Exception;
use PHPUnit\Framework\TestCase;

class TestDay3 extends TestCase
{
    public function testCalc(): void
    {

        self::assertEquals(4138664, (new Day3())->calcResult(), 'got invalid result for ' . __FUNCTION__);
    }

    /**
     * @throws Exception
     */
    public function testCalc2(): void
    {
        self::assertEquals(4273224, (new Day3())->calcResult2(), 'got invalid result for ' . __FUNCTION__);
    }
}
