<?php

declare(strict_types=1);

namespace Unit;

use AdventOfCode\Riddles\Day14;
use PHPUnit\Framework\TestCase;

class TestDay14 extends TestCase
{
    public function testCalc(): void
    {
        self::assertEquals(6687, (new Day14())->calcResult(), 'got invalid result for ' . __FUNCTION__);
    }

    public function testCalc2(): void
    {
        self::assertEquals(19851, (new Day14())->calcResult2(), 'got invalid result for ' . __FUNCTION__);
    }
}
