<?php

declare(strict_types=1);

namespace Unit;

use AdventOfCode\Riddles\Day7;
use PHPUnit\Framework\TestCase;

class TestDay7 extends TestCase
{
    public function testCalc(): void
    {
        self::assertEquals(341534, (new Day7())->calcResult(), 'got invalid result for ' . __FUNCTION__);
    }

    public function testCalc2(): void
    {
        self::assertEquals(93397632, (new Day7())->calcResult2(), 'got invalid result for ' . __FUNCTION__);
    }
}
