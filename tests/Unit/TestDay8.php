<?php

declare(strict_types=1);

namespace Unit;

use AdventOfCode\Riddles\Day8;
use PHPUnit\Framework\TestCase;

class TestDay8 extends TestCase
{
    public function testCalc(): void
    {
        self::assertEquals(512, (new Day8())->calcResult(), 'got invalid result for ' . __FUNCTION__);
    }

    public function testCalc2(): void
    {
        self::assertEquals(1091165, (new Day8())->calcResult2(), 'got invalid result for ' . __FUNCTION__);
    }
}
