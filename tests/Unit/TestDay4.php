<?php

declare(strict_types=1);

namespace Unit;

use AdventOfCode\Riddles\Day4;
use PHPUnit\Framework\TestCase;

class TestDay4 extends TestCase
{
    public function testCalc(): void
    {
        self::assertEquals(54275, (new Day4())->calcResult(), 'got invalid result for ' . __FUNCTION__);
    }

    public function testCalc2(): void
    {
        self::assertEquals(13158, (new Day4())->calcResult2(), 'got invalid result for ' . __FUNCTION__);
    }
}
