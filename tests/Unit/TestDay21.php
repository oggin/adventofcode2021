<?php

declare(strict_types=1);

namespace Unit;

use AdventOfCode\Riddles\Day21;
use PHPUnit\Framework\TestCase;

class TestDay21 extends TestCase
{
    public function testCalc(): void
    {
        self::assertEquals(6687, (new Day21())->calcResult(), 'got invalid result for ' . __FUNCTION__);
    }

    public function testCalc2(): void
    {
        self::assertEquals(19851, (new Day21())->calcResult2(), 'got invalid result for ' . __FUNCTION__);
    }
}
