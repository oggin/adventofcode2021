<?php

declare(strict_types=1);

namespace Unit;

use AdventOfCode\Riddles\Day1;
use PHPUnit\Framework\TestCase;

class TestDay1 extends TestCase
{
    public function testDayFirst(): void
    {
        self::assertEquals(1266, (new Day1())->calcResult(), 'got invalid result for first riddle');
    }

    public function testDaySecond(): void
    {
        self::assertEquals(1217, (new Day1())->calcResult2(), 'got invalid result for second riddle');
    }
}
