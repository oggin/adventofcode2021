<?php

declare(strict_types=1);

namespace Unit;

use AdventOfCode\Riddles\Day2;
use Exception;
use PHPUnit\Framework\TestCase;

class TestDay2 extends TestCase
{
    /**
     * @throws Exception
     */
    public function testCalcWithoutAim(): void
    {
        self::assertEquals(1762050, (new Day2())->calcResult(), 'got invalid result for ' . __FUNCTION__);
    }

    /**
     * @throws Exception
     */
    public function testCalcWithAim(): void
    {
        self::assertEquals(1855892637, (new Day2())->calcResult2(), 'got invalid result for ' . __FUNCTION__);
    }
}
