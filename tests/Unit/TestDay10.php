<?php

declare(strict_types=1);

namespace Unit;

use AdventOfCode\Riddles\Day10;
use PHPUnit\Framework\TestCase;

class TestDay10 extends TestCase
{
    public function testCalc(): void
    {
        self::assertEquals(311895, (new Day10())->calcResult(), 'got invalid result for ' . __FUNCTION__);
    }

    public function testCalc2(): void
    {
        self::assertEquals(2904180541, (new Day10())->calcResult2(), 'got invalid result for ' . __FUNCTION__);
    }
}
