<?php

declare(strict_types=1);

namespace Unit;

use AdventOfCode\Riddles\Day11;
use PHPUnit\Framework\TestCase;

class TestDay11 extends TestCase
{
    public function testCalc(): void
    {
        self::assertEquals(1725, (new Day11())->calcResult(), 'got invalid result for ' . __FUNCTION__);
    }

    public function testCalc2(): void
    {
        self::assertEquals(308, (new Day11())->calcResult2(), 'got invalid result for ' . __FUNCTION__);
    }
}
