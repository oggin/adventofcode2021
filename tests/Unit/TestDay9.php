<?php

declare(strict_types=1);

namespace Unit;

use AdventOfCode\Riddles\Day9;
use PHPUnit\Framework\TestCase;

class TestDay9 extends TestCase
{
    public function testCalc(): void
    {
        self::assertEquals(480, (new Day9())->calcResult(), 'got invalid result for ' . __FUNCTION__);
    }

    public function testCalc2(): void
    {
        self::assertEquals(1045660, (new Day9())->calcResult2(), 'got invalid result for ' . __FUNCTION__);
    }
}
