<?php

declare(strict_types=1);

namespace Unit;

use AdventOfCode\Riddles\Day6;
use PHPUnit\Framework\TestCase;

class TestDay6 extends TestCase
{
    public function testCalc(): void
    {
        self::assertEquals(386536, (new Day6())->calcResult(), 'got invalid result for ' . __FUNCTION__);
    }

    public function testCalc2(): void
    {
        self::assertEquals(1732821262171, (new Day6())->calcResult2(), 'got invalid result for ' . __FUNCTION__);
    }
}
